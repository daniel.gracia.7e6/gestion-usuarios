import java.io.File
import java.nio.file.Path
import java.text.SimpleDateFormat
import java.util.*
import kotlin.io.path.*


val userDataPath = Path("./src/main/resources/data/userData.txt")

/**
 * Devuelve si el directorio esta vacio o no
 * @param files Lista de paths
 * @return True si el directorio contiene algún archivo
 * @author Daniel Gracia Quiroga
 * @version 24/1/23
 */
fun directoryNotEmpty(files: List<Path>): Boolean{
    return files.isNotEmpty()
}

/**
 * Comproba si exiteix un fitxer
 * @param path El path a comprobar
 * @return True si el fitxer existeix
 * @author Daniel Gracia Quiroga
 * @version 24/1/23
 */
fun fileExists(path: Path): Boolean {
    return path.exists()
}

/**
 * Passa a llista totes les linies de text d'un fitxer
 * @author Oriol Lladó Real
 * @version 24/1/23
 * @param file És el fitxer que li passem oer llegir el fitxer
 * @return Retorna una llista amb totes les lines del fitxer
 */
fun readData(file: Path): List<String> {
    return file.readLines()
}

/**
 * Elimina el fitxer introduït
 * @author Oriol Lladó Real
 * @version 24/1/23
 * @param path És la ruta del fitxer que hem d'eliminar
 */
fun deleteFile(path: String){
    val file = File(path)
    if (file.exists()) {
        file.delete()
        println("file deleted")
    }
}

/**
 * Busca quin és l'últim id que hi ha al fitxer i et torna el següent
 * @author Oriol Lladó Real
 * @author Daniel Gracia Quiroga
 * @version 24/1/23
 * @return Retorna l'últim id que hi ha al fitxer
 */
fun lastID(): Int{
    if (userDataIsEmpty()) return 0
    val lastUser = (readData(userDataPath)[readData(userDataPath).size-1])
    val id = lastUser.split(";").toMutableList()[0]
    return id.toInt() + 1
}

/**
 * Comprova si userData està buit
 * @author Daniel Gracia Quiroga
 * @author Oriol Lladó Real
 * @version 24/1/23
 * @return retorna True si el fitxer està buit
 */
fun userDataIsEmpty(): Boolean{
    val file = File(userDataPath.toString())
    return (file.length() == 0L)
}


/**
 * Formata els usuaris i els converteix en una llista
 * @author Daniel Gracia Quiroga
 * @version 24/1/23
 * @return retorna una llista amb els usuaris
 */
fun formatData(lines: List<String>): MutableList<List<String>> {
    val userList: MutableList<List<String>> = mutableListOf()

    var idCount = lastID()

    for(user in lines) {
        val userFormat = user.substring(0, user.length - 1).split(",").toMutableList()
        if(userFormat[4] != "false" && userFormat[5] != "true") {
            userFormat[0] = idCount.toString()
            idCount++
            userList.add(userFormat)
        }
    }

    return userList
}

/**
 * Importa els usuaris i els guarda al fitxer userData.txt
 * @author Daniel Gracia Quiroga
 * @version 24/1/23
 * @return Retorna true al finalitzar
 */
fun importUsers(users: MutableList<List<String>>): Boolean {
    for(user in users) {
        //Replace , for ; and remove blank spaces
        var userFormat = user.toString().replace(", ", ";")
        userFormat = userFormat.replace("[", "").replace("]", "")
        //Remove []
        userDataPath.appendText("$userFormat\n")
    }
    return true
}

/**
 * Retorna la data d'avui formatjada
 * @author Daniel Gracia Quiroga
 * @version 30/1/23
 * @return Retorna string amb la data
 */
fun todayDate(): String {
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
    return simpleDateFormat.format( Date())
}

/**
 * Realitza un backup
 * @author Daniel Gracia Quiroga
 * @version 30/1/23
 * @return Retorna true al finalitzar
 */
fun backup(): Boolean {
    val filename = Path("./src/main/resources/backup/${todayDate()}-userData.txt")
    filename.deleteIfExists()
    userDataPath.copyTo(filename)

    println("Backup realitzat correctament.")

    return true
}

/**
 * Comprova si existeix un backup pel dia d'avui
 * @author Daniel Gracia Quiroga
 * @version 30/1/23
 * @return Retorna true si existeix
 */
fun todayBackupExists(): Boolean{
    val filename = Path("./src/main/resources/backup/${todayDate()}-userData.txt")
    return fileExists(filename)
}

/**
 * Mostra el menu de l'aplicació
 * @author Oriol Lladó Real
 * @version 30/1/23
 * @return Retorna l'opció seleccionada per l'usuari
 */
fun menu(): Int{

    println("""
        QUE VOLS FER?
        1. Crear Usuari
        2. Modificar Usuari
        3. Des/Bloquejar Usuari
        4. Importar dades
        5. Fer Backup
        6. Sortir
    """.trimIndent())
    print("Tria una opció: ")
    return readln().toInt()
}

/**
 * Demana a l'usuari nom cognom, telèfon i mail i afegeix l'usuari al fitxer
 * @author Oriol Lladó Real
 * @version 30/1/23
 */
fun createUser(){
    print("Nom i cognom: ")
    val nom = readln()
    print("Telefon: ")
    val tel = readln()
    print("Mail: ")
    val mail = readln()
    val id = lastID()
    val data = "$id;$nom;$tel;$mail;true;false"
    userDataPath.appendText("$data\n")
}
/**
 * Modifica una línia específica del fitxer
 * @author Oriol Lladó Real
 * @version 30/1/23
 * @param filePath ruta del fitxer que volem modificar
 * @param lineNumber numero de linea que volem modificar
 * @param newLine la línia que volem afegir
 */
fun modifyLine(filePath: Path, lineNumber: Int, newLine: String) {
    val lines = filePath.readLines().toMutableList()
    lines[lineNumber] = newLine
    filePath.writeText(lines.joinToString(separator = "\n") + "\n")
}

/**
 * Extreu la línia corresponent a la posició de la llista on hi ha el id
 * @author Oriol Lladó Real
 * @version 31/1/23
 * @param filePath ruta del fitxer que volem modificar
 * @param id numero de id per extreure la seva position correspondent
 * @return retorna un enter el qual correspon a la posició de la llista on hi ha al id corresponent si no existeix retorna -1
 */
fun extractPosition(filePath: Path, id: Int): Int{
    val lines = filePath.readLines().toMutableList()
    for (e in lines){
        val identifier = e.split(';')[0].toInt()
        if(id == identifier) return lines.indexOf(e)
    }
    return -1
}

/**
 * Demana un id del qual vols modificar les dades, si aquests id existeix, demana les noves dades a introduir i les canvia.
 * @author Oriol Lladó Real
 * @version 31/1/23
 */
fun modifyUser(){
    val informacio = readData(userDataPath)
    print("ID del usuari que es vol modificar--> ")
    val id = readln()
    val lineNumber = extractPosition(userDataPath, id.toInt())
    println(lineNumber)

    if (lineNumber != -1) {

        val element = informacio[lineNumber]
        val data = element.split(";").toMutableList()
        println("Aquestes son les dades")
        println("ID--> ${data[0]}")
        println("Nom i cognom--> ${data[1]}")
        println("Telèfon--> ${data[2]}")
        println("Mail--> ${data[3]}")
        println()
        println("MODIFICA LES DADES")
        print("Nom i cognom: ")
        val nom = readln()
        print("Telefon: ")
        val tel = readln()
        print("Mail: ")
        val mail = readln()
        data[1] = nom
        data[2] = tel
        data[3] = mail
        val new = data.joinToString(";")

        modifyLine(userDataPath, lineNumber, new)
    } else{
        println("El id introduït es inexistent.")
    }

}

/**
 * Demana un id del qual vols modificar les dades, si aquests id existeix, canvia l'estat del boolean. De true a false o de false a true respectivament
 * @author Oriol Lladó Real
 * @version 31/1/23
 */
fun block(){
    val information = readData(userDataPath)
    print("ID del usuari que es vol block/desblock--> ")
    val id = readln()
    val lineNumber = extractPosition(userDataPath, id.toInt())
    println(lineNumber)

    if (lineNumber != -1) {
        val element = information[lineNumber]
        val data = element.split(";").toMutableList()
        data[5] = (!data[5].toBoolean()).toString()
        val new = data.joinToString(";")
        modifyLine(userDataPath, id.toInt(), new)
        println("Usuari actualitzat.")
    }else{
        println("Id no acceptat.")
    }
}


/**
 * Importa els usuaris del directori import
 * @author Daniel Gracia Quiroga
 * @version 31/1/23
 */
fun checkImport() {
    val importPath = Path("./src/main/resources/import/")
    val files : List<Path> = importPath.listDirectoryEntries()
    if (directoryNotEmpty(files)){
        for (file in files){
            val users = formatData(readData(file))
            importUsers(users)
            deleteFile(file.toString())
        }
        println("Dades importades correctament.")
    } else {
        println("No hi ha dades per importar.")
    }


}

fun main(){
    //Comprovar si hi ha una cosa en import, i fa una importació
    checkImport()
    var op = menu()
    while (op != 6){
        when(op){
            1 -> createUser()
            2 -> modifyUser()
            3 -> block()
            4 -> checkImport()
            5 -> backup()
        }
        op = menu()
    }

    //Realize backup
    if(!todayBackupExists()) {
        backup()
    }
}
