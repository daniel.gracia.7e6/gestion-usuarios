# Sistema de gestió d'usuaris per al gimnàs ASMA

Aquest projecte té com a objectiu implementar un sistema per donar d'alta i gestionar usuaris en un gimnàs del grup ASMA. El sistema permetrà enregistrar nous usuaris i actualitzacions sobre ells, i serà compatible amb les dades del sistema antic.

## Estructura del projecte

El projecte inclou tres directoris addicionals:

- `import`: conté els fitxers a importar del sistema antic.
- `backups`: conté els fitxers de backup.
- `data`: conté el fitxer `userData`.

## Importació de dades

El sistema comprovarà si hi ha fitxers per carregar en el directori `import` cada vegada que s'inicia l'aplicació. Si hi ha fitxers, es realitzarà una lectura i processament de les dades, i finalment esborrarà el fitxer per evitar carregar-lo de nou en futures execucions. Aquesta acció també es pot realitzar a través de l'opció del menú.

Les dades dels usuaris venen en format de fitxer, amb camps separats per comes `,` i usuaris separats per punt i coma `;`. El camp `id` no es traspassarà al nou sistema, ja que es crearà un nou `id` seqüencial per cada usuari enregistrat. Els usuaris que tinguin `actiu` a `false` i `blocked` a `true` no seran carregats al nou sistema.

Exemple:

    12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;
    123FG34,Lara Gomez,689953247,lara1985@mail.com,false,true;

## Funcionalitat del sistema

El sistema registra informació sobre usuaris en un fitxer de text. Abans d'executar qualsevol acció, es llegeix la informació previament registrada per determinar l'últim ID d'usuari utilitzat, el qual és un número seqüencial que comença a 1 i incrementa en 1 per cada nou usuari registrat.

Les funcions del sistema són representades en un menú i inclouen:
1. `Crear usuari`: Registra un nou usuari introduint el nom, telèfon i email i assignant-li un ID seqüencial.
2. `Modificar usuari`: Permet modificar les dades d'un usuari existent mitjançant l'introducció de la seva ID. Si l'ID existeix, les dades s'actualitzen, en cas contrari, es mostrarà un missatge d'error.
3. `Des/Bloquejar usuari`: Similar a la funció de modificar usuari, permet bloquejar o desbloquejar un usuari existent mitjançant la seva ID.
4. `Sortir`: Opció per sortir del programa.



## Funcions
Aquest projecte conté les següents funcions:
- `directoryNotEmpty`: retorna si el directori està buit o no
- `fileExists`: comprova si existeix un fitxer
- `readData`: passa a llista totes les línies de text d'un fitxer
- `deleteFile`: elimina el fitxer introduït
- `lastID`: busca quin és l'últim ID que hi ha al fitxer i et torna el següent
- `userDataIsEmpty`: comprova si el fitxer userData està buit
- `formatData`: formata els usuaris i els converteix en una llista
- `importUsers`: importa els usuaris i els guarda al fitxer userData.txt
- `todayDate`: retorna la data d'avui formatjada

La documentació amb [KDoc](https://kotlinlang.org/docs/kotlin-doc.html) de les funcions és un HTML el qual es torba a: `build/dokka/index.html`
## Autors
- Daniel Gracia Quiroga
- Oriol Lladó Real

## Versió
1.0
